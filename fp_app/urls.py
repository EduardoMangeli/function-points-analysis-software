from django.urls import path

from .views import index_view, project_views, client_views, \
    boundary_views, data_function_views, transactional_function_views, \
    counting_views, operation_views


app_name = 'fp_app'
urlpatterns = [
    path('', index_view.index, name='index'),

    path('project/', project_views.IndexView.as_view(), name='project_index'),
    path('project/<int:pk>/', project_views.DetailView.as_view(), name='project_detail'),
    path('project/new', project_views.CreateView.as_view(), name='project_new'),
    path('project/delete/<int:pk>', project_views.DeleteView.as_view(), name='project_delete'),
    path('project/update/<int:pk>', project_views.UpdateView.as_view(), name='project_update'),
    
    path('client/', client_views.IndexView.as_view(), name='client_index'),
    path('client/<int:pk>/', client_views.DetailView.as_view(), name='client_detail'),
    path('client/new', client_views.CreateView.as_view(), name='client_new'),
    path('client/delete/<int:pk>', client_views.DeleteView.as_view(), name='client_delete'),
    path('client/update/<int:pk>', client_views.UpdateView.as_view(), name='client_update'),
    
    path('boundary/', boundary_views.IndexView.as_view(), name='boundary_index'),
    path('boundary/<int:pk>/', boundary_views.DetailView.as_view(), name='boundary_detail'),
    path('boundary/new', boundary_views.CreateView.as_view(), name='boundary_new'),
    path('boundary/delete/<int:pk>', boundary_views.DeleteView.as_view(), name='boundary_delete'),
    path('boundary/update/<int:pk>', boundary_views.UpdateView.as_view(), name='boundary_update'),
    
    path('data-function/', data_function_views.IndexView.as_view(), name='data_function_index'),
    path('data-function/<int:pk>/', data_function_views.DetailView.as_view(), name='data_function_detail'),
    path('data-function/new', data_function_views.CreateView.as_view(), name='data_function_new'),
    path('data-function/delete/<int:pk>', data_function_views.DeleteView.as_view(), name='data_function_delete'),
    path('data-function/update/<int:pk>', data_function_views.UpdateView.as_view(), name='data_function_update'),
    
    path('transactional-function/', transactional_function_views.IndexView.as_view(),
         name='transactional_function_index'),
    path('transactional-function/<int:pk>/', transactional_function_views.DetailView.as_view(),
         name='transactional_function_detail'),
    path('transactional-function/new', transactional_function_views.CreateView.as_view(),
         name='transactional_function_new'),
    path('transactional-function/delete/<int:pk>', transactional_function_views.DeleteView.as_view(),
         name='transactional_function_delete'),
    path('transactional-function/update/<int:pk>', transactional_function_views.UpdateView.as_view(),
         name='transactional_function_update'),
    
    path('counting/', counting_views.IndexView.as_view(), name='counting_index'),
    path('counting/<int:pk>/', counting_views.DetailView.as_view(), name='counting_detail'),
    path('counting/new', counting_views.CreateView.as_view(), name='counting_new'),
    path('counting/delete/<int:pk>', counting_views.DeleteView.as_view(), name='counting_delete'),
    path('counting/update/<int:pk>', counting_views.UpdateView.as_view(), name='counting_update'),

    path('operation/', operation_views.IndexView.as_view(), name='operation_index'),
    path('operation/<int:pk>/', operation_views.DetailView.as_view(), name='operation_detail'),
    path('operation/new', operation_views.CreateView.as_view(), name='operation_new'),
    path('operation/delete/<int:pk>', operation_views.DeleteView.as_view(), name='operation_delete'),
    path('operation/update/<int:pk>', operation_views.UpdateView.as_view(), name='operation_update'),
    
]