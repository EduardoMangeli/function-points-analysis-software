from django import template

register = template.Library()


@register.simple_tag
def model_name(value):
    if hasattr(value, 'model'):
        value = value.model
    elif hasattr(value, 'view'):
        value = value.view.model

    return value._meta.verbose_name.title()