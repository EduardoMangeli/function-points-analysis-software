from fp_app.views.fp_app_generic_views import FPIndexView, FPDetailView
from django.urls import reverse_lazy
from django.views import generic

from fp_app.models import Project

from fp_app.tables import ProjectTable

from fp_app.filters import ProjectFilter


class IndexView(FPIndexView):
    extra_context = {
        'title': 'Projects',
        'self_url': reverse_lazy('fp_app:project_index'),
        'create_url': reverse_lazy('fp_app:project_new'),
    }
    model = Project
    table_class = ProjectTable
    filterset_class = ProjectFilter


class DetailView(FPDetailView):
    extra_context = {
        'title': 'Project Detail',
        'index_url': reverse_lazy('fp_app:project_index'),
        'update_url_str': 'fp_app:project_update',
    }
    model = Project


class CreateView(generic.CreateView):
    extra_context = {'title': 'New Project'}
    model = Project
    fields = ['description', 'short_description', 'client', 'parent']
    template_name = 'form_generic.html'

    def get_success_url(self):
        return reverse_lazy('fp_app:project_detail', args=[self.object.id])


class DeleteView(generic.DeleteView):
    model = Project
    extra_context = {'title': 'Delete Project'}
    template_name = 'confirm_delete_generic.html'
    success_url = reverse_lazy('fp_app:project_index')


class UpdateView(generic.UpdateView):
    model = Project
    extra_context = {'title': 'Update Project'}
    fields = ['description', 'short_description', 'client', 'parent']
    template_name = 'form_generic.html'

    def get_success_url(self):
        return reverse_lazy('fp_app:project_detail', args=[self.object.id])