from django_tables2 import SingleTableMixin
from django_filters.views import FilterView
from django.views import generic
from django.core.exceptions import ObjectDoesNotExist


class FPIndexView(SingleTableMixin, FilterView):
    template_name = 'index_generic.html'
    table_pagination = {
        "per_page": 5
    }

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['has_filter'] = any(field in self.request.GET for field in set(self.filterset_class.get_fields()))
        return context


class FPDetailView(generic.DetailView):
    template_name = 'detail_generic.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['fields_name_and_value'] = []
        #context['model'] = self.model
        # noinspection PyProtectedMember
        for field in self.model._meta.get_fields():
            if field.concrete:
                field_name = field.verbose_name
                field_value = field.value_from_object(self.get_object())
                try:
                    if field.is_relation:
                        if type(field_value) == list:
                            vs = []
                            for v in field_value:
                                vs += [str(v)]
                            field_value = vs
                        else:
                            field_value = field.related_model.objects.get(pk=field_value)
                except ObjectDoesNotExist:
                    continue
                context['fields_name_and_value'] += [(field_name, field_value)]
        return context

