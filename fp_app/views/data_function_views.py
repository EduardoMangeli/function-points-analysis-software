from fp_app.views.fp_app_generic_views import FPIndexView, FPDetailView
from django.urls import reverse_lazy
from django.views import generic

from fp_app.models import DataFunction

from fp_app.tables import DataFunctionTable

from fp_app.filters import DataFunctionFilter


class IndexView(FPIndexView):
    extra_context = {
        'title': 'Data Functions',
        'self_url': reverse_lazy('fp_app:data_function_index'),
        'create_url': reverse_lazy('fp_app:data_function_new'),
    }
    model = DataFunction
    table_class = DataFunctionTable
    filterset_class = DataFunctionFilter


class DetailView(FPDetailView):
    extra_context = {
        'title': 'Data Function Detail',
        'index_url': reverse_lazy('fp_app:data_function_index'),
        'update_url_str': 'fp_app:data_function_update',
        'exclude_fields': ['polymorphic ctype'],
    }

    model = DataFunction


class CreateView(generic.CreateView):
    extra_context = {'title': 'New Data Function',}
    model = DataFunction
    fields = '__all__'
    template_name = 'form_generic.html'

    def get_success_url(self):
        return reverse_lazy('fp_app:data_function_detail', args=[self.object.id])


class DeleteView(generic.DeleteView):
    model = DataFunction
    extra_context = {'title': 'Delete Data Function'}
    template_name = 'confirm_delete_generic.html'
    success_url = reverse_lazy('fp_app:data_function_index')


class UpdateView(generic.UpdateView):
    model = DataFunction
    extra_context = {'title': 'Delete Data Function'}
    fields = '__all__'
    template_name = 'form_generic.html'

    def get_success_url(self):
        return reverse_lazy('fp_app:data_function_detail', args=[self.object.id])