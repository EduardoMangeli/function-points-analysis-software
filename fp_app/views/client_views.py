from fp_app.views.fp_app_generic_views import FPIndexView, FPDetailView
from django.urls import reverse_lazy
from django.views import generic


from fp_app.models import Client

from fp_app.tables import ClientTable

from fp_app.filters import ClientFilter


class IndexView(FPIndexView):
    extra_context = {
        'title': 'Clients',
        'self_url': reverse_lazy('fp_app:client_index'),
        'create_url': reverse_lazy('fp_app:client_new'),
    }
    model = Client
    table_class = ClientTable
    filterset_class = ClientFilter


class DetailView(FPDetailView):
    extra_context = {
        'title': 'Client Detail',
        'index_url': reverse_lazy('fp_app:client_index'),
        'update_url_str': 'fp_app:client_update',
    }
    model = Client


class CreateView(generic.CreateView):
    extra_context = {'title': 'New Client'}
    model = Client
    fields = ['description', 'short_description']
    template_name = 'form_generic.html'

    def get_success_url(self):
        return reverse_lazy('fp_app:client_detail', args=[self.object.id])


class DeleteView(generic.DeleteView):
    model = Client
    extra_context = {'title': 'Delete Client'}
    template_name = 'confirm_delete_generic.html'
    success_url = reverse_lazy('fp_app:client_index')


class UpdateView(generic.UpdateView):
    model = Client
    extra_context = {'title': 'Update Client'}
    fields = ['description', 'short_description']
    template_name = 'form_generic.html'

    def get_success_url(self):
        return reverse_lazy('fp_app:client_detail', args=[self.object.id])
