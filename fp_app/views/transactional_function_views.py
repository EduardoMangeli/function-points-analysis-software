from fp_app.views.fp_app_generic_views import FPIndexView, FPDetailView
from django.urls import reverse_lazy
from django.views import generic

from fp_app.models import TransactionalFunction

from fp_app.tables import TransactionalFunctionTable

from fp_app.filters import TransactionalFunctionFilter


class IndexView(FPIndexView):
    extra_context = {
        'title': 'Transactional Functions',
        'self_url': reverse_lazy('fp_app:transactional_function_index'),
        'create_url': reverse_lazy('fp_app:transactional_function_new'),
    }
    model = TransactionalFunction
    table_class = TransactionalFunctionTable
    filterset_class = TransactionalFunctionFilter


class DetailView(FPDetailView):
    extra_context = {
        'title': 'Transactional Function Detail',
        'index_url': reverse_lazy('fp_app:transactional_function_index'),
        'update_url_str': 'fp_app:transactional_function_update',
        'exclude_fields': ['polymorphic ctype'],
    }
    model = TransactionalFunction


class CreateView(generic.CreateView):
    extra_context = {'title': 'New Transactional Function',}
    model = TransactionalFunction
    fields = '__all__'
    template_name = 'form_generic.html'

    def get_success_url(self):
        return reverse_lazy('fp_app:transactional_function_detail', args=[self.object.id])


class DeleteView(generic.DeleteView):
    model = TransactionalFunction
    extra_context = {'title': 'Delete Transactional Function'}
    template_name = 'confirm_delete_generic.html'
    success_url = reverse_lazy('fp_app:transactional_function_index')


class UpdateView(generic.UpdateView):
    model = TransactionalFunction
    extra_context = {'title': 'Update Transactional Function'}
    fields = '__all__'
    template_name = 'form_generic.html'

    def get_success_url(self):
        return reverse_lazy('fp_app:transactional_function_detail', args=[self.object.id])
