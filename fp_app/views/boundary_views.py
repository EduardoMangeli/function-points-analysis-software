from fp_app.views.fp_app_generic_views import FPIndexView, FPDetailView
from django.urls import reverse_lazy
from django.views import generic


from fp_app.models import Boundary

from fp_app.tables import BoundaryTable

from fp_app.filters import BoundaryFilter


class IndexView(FPIndexView):
    extra_context = {
        'title': 'Boundaries',
        'self_url': reverse_lazy('fp_app:boundary_index'),
        'create_url': reverse_lazy('fp_app:boundary_new'),
    }
    model = Boundary
    table_class = BoundaryTable
    filterset_class = BoundaryFilter


class DetailView(FPDetailView):
    extra_context = {
        'title': 'Boundary Detail',
        'index_url': reverse_lazy('fp_app:boundary_index'),
        'update_url_str': 'fp_app:boundary_update',
    }
    model = Boundary


class CreateView(generic.CreateView):
    extra_context = {'title': 'New Boundary'}
    model = Boundary
    fields = ['description', 'short_description', 'project']
    template_name = 'form_generic.html'

    def get_success_url(self):
        return reverse_lazy('fp_app:boundary_detail', args=[self.object.id])


class DeleteView(generic.DeleteView):
    model = Boundary
    extra_context = {'title': 'Delete Boundary'}
    template_name = 'confirm_delete_generic.html'
    success_url = reverse_lazy('fp_app:boundary_index')


class UpdateView(generic.UpdateView):
    model = Boundary
    extra_context = {'title': 'Delete Boundary'}
    fields = ['description', 'short_description', 'project']
    template_name = 'form_generic.html'

    def get_success_url(self):
        return reverse_lazy('fp_app:boundary_detail', args=[self.object.id])