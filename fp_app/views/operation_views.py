from fp_app.views.fp_app_generic_views import FPIndexView, FPDetailView

from django.urls import reverse_lazy
from django.views import generic

from fp_app.models import Operation

from fp_app.tables import OperationTable

from fp_app.filters import OperationFilter


class IndexView(FPIndexView):
    extra_context = {
        'title': 'Operations',
        'self_url': reverse_lazy('fp_app:operation_index'),
        'create_url': reverse_lazy('fp_app:operation_new'),
    }
    model = Operation
    table_class = OperationTable
    filterset_class = OperationFilter


class DetailView(FPDetailView):
    extra_context = {
        'title': 'Operation Detail',
        'index_url': reverse_lazy('fp_app:operation_index'),
        'update_url_str': 'fp_app:operation_update',
    }
    model = Operation


class CreateView(generic.CreateView):
    extra_context = {'title': 'New Operation'}
    model = Operation
    fields = ['counting', 'type', 'countable', 'qtd', 'fp']
    template_name = 'form_generic.html'

    def get_success_url(self):
        return reverse_lazy('fp_app:operation_detail', args=[self.object.id])


class DeleteView(generic.DeleteView):
    model = Operation
    extra_context = {'title': 'Delete Operation'}
    template_name = 'confirm_delete_generic.html'
    success_url = reverse_lazy('fp_app:operation_index')


class UpdateView(generic.UpdateView):
    model = Operation
    extra_context = {'title': 'Update Operation'}
    fields = ['counting', 'type', 'countable', 'qtd', 'fp']
    template_name = 'form_generic.html'

    def get_success_url(self):
        return reverse_lazy('fp_app:operation_detail', args=[self.object.id])
