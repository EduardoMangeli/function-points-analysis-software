from fp_app.views.fp_app_generic_views import FPIndexView, FPDetailView

from django import forms

from django.urls import reverse_lazy
from django.views import generic

from fp_app.models import Counting

from fp_app.tables import CountingTable

from fp_app.filters import CountingFilter


class IndexView(FPIndexView):
    extra_context = {
        'title': 'Counts',
        'self_url': reverse_lazy('fp_app:counting_index'),
        'create_url': reverse_lazy('fp_app:counting_new'),
    }
    model = Counting
    table_class = CountingTable
    filterset_class = CountingFilter


class DetailView(FPDetailView):
    extra_context = {
        'title': 'Counting Detail',
        'index_url': reverse_lazy('fp_app:counting_index'),
        'update_url_str': 'fp_app:counting_update',
    }
    model = Counting


class CreateView(generic.CreateView):
    extra_context = {'title': 'New Counting'}
    model = Counting
    fields = ['description', 'project', 'date']
    template_name = 'form_generic.html'

    def get_success_url(self):
        return reverse_lazy('fp_app:counting_detail', args=[self.object.id])

    def get_form(self, form_class=None):
        form = super(CreateView, self).get_form(form_class)
        form.fields['date'] = forms.DateField(widget=forms.TextInput(
            attrs={'class': 'datepicker'}
        ))
        return form


class DeleteView(generic.DeleteView):
    model = Counting
    extra_context = {'title': 'Delete Counting'}
    template_name = 'confirm_delete_generic.html'
    success_url = reverse_lazy('fp_app:counting_index')


class UpdateView(generic.UpdateView):
    model = Counting
    extra_context = {
        'title': 'Update Counting',
        'disabled_fields': ['project'],
    }
    fields = ['description', 'project', 'date']
    widgets = {
        'project': forms.Textarea(),
    }
    template_name = 'form_generic.html'

    def get_success_url(self):
        return reverse_lazy('fp_app:counting_detail', args=[self.object.id])

    def get_form(self, form_class=None):
        form = super(UpdateView, self).get_form(form_class)
        form.fields['date'] = forms.DateField(widget=forms.TextInput(
            attrs={'class': 'datepicker'}
        ))
        return form

