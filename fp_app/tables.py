import django_tables2 as tables
from django.utils.html import format_html
from django.urls import reverse
from .models import Project, Client, Boundary, DataFunction, TransactionalFunction, Counting, Operation


class BaseTable(tables.Table):
    controls = tables.Column(empty_values=(), orderable=False, attrs={"cell": {'class': "text-center"}})

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.update_view = ''
        self.delete_view = ''
        self.detail_view = ''

        for col in self.base_columns:
            if ('cell' in self.base_columns[col].attrs) and ('class' in self.base_columns[col].attrs['cell']):
                self.base_columns[col].attrs['cell']['class'] += " align-middle"
            elif 'cell' in self.base_columns[col].attrs:
                self.base_columns[col].attrs['cell'].update({'class': 'align-middle'})
            else:
                self.base_columns[col].attrs['cell'] = {'class': 'align-middle'}

    def render_controls(self, record):
        detail = "<a href='%s'><i class='fas fa-info' data-toggle='tooltip' title='detail'></i></a>" % reverse(
            self.detail_view,  args=[record.id])
        update = "<a href='%s'><i class='fas fa-pen' data-toggle='tooltip' title='update'></i></a>" % reverse(
            self.update_view, args=[record.id])
        delete = "<a href='%s'><i class='fas fa-trash-alt' data-toggle='tooltip' title='delete'></i></a>" % reverse(
            self.delete_view, args=[record.id])
        return format_html(detail + ' &nbsp; ' + update + ' &nbsp; ' + delete)

    class Meta:
        template_name = 'table.html'
        attrs = {'class': 'table table-sm table-hover',}
        sequence = ('...', 'controls')


class ProjectTable(BaseTable):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.update_view = 'fp_app:project_update'
        self.delete_view = 'fp_app:project_delete'
        self.detail_view = 'fp_app:project_detail'

    class Meta(BaseTable.Meta):
        model = Project


class ClientTable(BaseTable):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.update_view = 'fp_app:client_update'
        self.delete_view = 'fp_app:client_delete'
        self.detail_view = 'fp_app:client_detail'

    class Meta(BaseTable.Meta):
        model = Client


class BoundaryTable(BaseTable):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.update_view = 'fp_app:boundary_update'
        self.delete_view = 'fp_app:boundary_delete'
        self.detail_view = 'fp_app:boundary_detail'

    class Meta(BaseTable.Meta):
        model = Boundary
        
        
class DataFunctionTable(BaseTable):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.update_view = 'fp_app:data_function_update'
        self.delete_view = 'fp_app:data_function_delete'
        self.detail_view = 'fp_app:data_function_detail'

    class Meta(BaseTable.Meta):
        model = DataFunction
        exclude = ['polymorphic_ctype', 'countable_ptr']


class TransactionalFunctionTable(BaseTable):
    file_type_referenced = tables.ManyToManyColumn(empty_values=())

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.update_view = 'fp_app:transactional_function_update'
        self.delete_view = 'fp_app:transactional_function_delete'
        self.detail_view = 'fp_app:transactional_function_detail'

    class Meta(BaseTable.Meta):
        model = TransactionalFunction
        exclude = ['polymorphic_ctype', 'countable_ptr']


class CountingTable(BaseTable):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.update_view = 'fp_app:counting_update'
        self.delete_view = 'fp_app:counting_delete'
        self.detail_view = 'fp_app:counting_detail'

    class Meta(BaseTable.Meta):
        model = Counting


class OperationTable(BaseTable):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.update_view = 'fp_app:operation_update'
        self.delete_view = 'fp_app:operation_delete'
        self.detail_view = 'fp_app:operation_detail'

    class Meta(BaseTable.Meta):
        model = Operation