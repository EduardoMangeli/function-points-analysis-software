import django_filters
from .models import Project, Client, Boundary, DataFunction, TransactionalFunction, Counting, Operation


class ProjectFilter(django_filters.FilterSet):
    description = django_filters.CharFilter(lookup_expr='icontains')

    class Meta:
        model = Project
        exclude = []


class ClientFilter(django_filters.FilterSet):
    description = django_filters.CharFilter(lookup_expr='icontains')

    class Meta:
        model = Client
        exclude = []


class BoundaryFilter(django_filters.FilterSet):
    description = django_filters.CharFilter(lookup_expr='icontains')

    class Meta:
        model = Boundary
        exclude = []


class DataFunctionFilter(django_filters.FilterSet):
    description = django_filters.CharFilter(lookup_expr='icontains')
    data_elements = django_filters.CharFilter(lookup_expr='icontains')
    record_elements = django_filters.CharFilter(lookup_expr='icontains')

    class Meta:
        model = DataFunction
        exclude = ['polymorphic_ctype']


class TransactionalFunctionFilter(django_filters.FilterSet):
    description = django_filters.CharFilter(lookup_expr='icontains')
    data_elements = django_filters.CharFilter(lookup_expr='icontains')

    class Meta:
        model = TransactionalFunction
        exclude = ['polymorphic_ctype']


class CountingFilter(django_filters.FilterSet):
    description = django_filters.CharFilter(lookup_expr='icontains')

    class Meta:
        model = Counting
        exclude = []


class OperationFilter(django_filters.FilterSet):
    class Meta:
        model = Operation
        exclude = []
