from django.contrib import admin

from polymorphic.admin import PolymorphicParentModelAdmin, PolymorphicChildModelAdmin

# Register your models here.
from .models import Client
from .models import Project
from .models import Boundary
from .models import Countable
from .models import DataFunction
from .models import TransactionalFunction
from .models import Counting
from .models import Operation
from .models import OperationType


class ModelDataFunctionChildAdmin(PolymorphicChildModelAdmin):
    base_model = DataFunction


class ModelTransactionalFunction(PolymorphicChildModelAdmin):
    base_model = TransactionalFunction


class ModelCountableParentAdmin(PolymorphicParentModelAdmin):
    base_model = Countable
    child_models = [DataFunction, TransactionalFunction]


admin.site.register(Client)
admin.site.register(Project)
admin.site.register(Boundary)
admin.site.register(DataFunction, ModelDataFunctionChildAdmin)
admin.site.register(TransactionalFunction, ModelTransactionalFunction)
admin.site.register(Countable, ModelCountableParentAdmin)
admin.site.register(Counting)
admin.site.register(Operation)
admin.site.register(OperationType)



