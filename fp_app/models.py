from django.db import models
from django_mysql.models import ListTextField
from polymorphic.models import PolymorphicModel
from django.db import DataError


# Create your models here.
from pyexpat import model


class Client(models.Model):
    description = models.CharField(max_length=100)
    short_description = models.CharField(max_length=20)

    def __str__(self):
        return self.description

    def __repr__(self):
        return '<Client %s>' % self.description


class Project(models.Model):
    description = models.CharField(max_length=200)
    short_description = models.CharField(max_length=10)
    client = models.ForeignKey('Client', on_delete=models.PROTECT, blank=True, null=True)
    parent = models.ForeignKey('Project', on_delete=models.PROTECT, blank=True, null=True)

    class Meta:
        constraints = [
            models.UniqueConstraint(fields=['description', 'client'], name='unique project description in client'),
            models.UniqueConstraint(fields=['short_description', 'client'],
                                    name='unique project short_description in client'),
        ]

    def __str__(self):
        return self.short_description

    def __repr__(self):
        return '<Project %s>' % self.description


class Boundary(models.Model):
    description = models.CharField(max_length=200)
    short_description = models.CharField(max_length=10)
    project = models.ForeignKey('Project', on_delete=models.PROTECT, blank=False, null=False)

    class Meta:
        constraints = [
            models.UniqueConstraint(fields=['short_description', 'project'], name='unique project boundary'),
        ]

    def __str__(self):
        return '%s | %s' % (self.project.short_description, self.short_description)

    def __repr__(self):
        return '<Boundary %s | %s>' % (self.description, self.project)


class Countable(PolymorphicModel):
    description = models.CharField(max_length=100, null=False, blank=False)
    boundary = models.ForeignKey('Boundary', on_delete=models.CASCADE, blank=False, null=False)
    data_elements = ListTextField(base_field=models.CharField(max_length=20))
    version = models.IntegerField(editable=False)
    creation_date = models.DateField(auto_now=True)

    class Meta:
        constraints = [
            models.UniqueConstraint(fields=['description', 'boundary', 'version'],
                                    name='unique Countable version in boundary'),
        ]

    def save(self, *args, **kwargs):
        current_version = Countable.objects.filter(
            description=self.description, boundary=self.boundary).order_by('-version')[:1]
        # increase version number
        self.version = current_version[0].version + 1 if current_version else 1
        # aways create new record
        # noinspection PyAttributeOutsideInit
        self.pk = None
        # noinspection PyAttributeOutsideInit
        self.id = None
        super(Countable, self).save(*args, **kwargs)

    def __str__(self):
        return self.get_real_instance().__str__()

    def __repr__(self):
        return self.get_real_instance().__repr__()


class DataFunction(Countable):
    DATA_FUNCTION_TYPE = [
        ('ILF', 'ILF | Internal Logical Field'),
        ('EIF', 'EIF | External Interface File')
    ]
    type = models.CharField(max_length=3, choices=DATA_FUNCTION_TYPE, null=False)
    record_elements = ListTextField(base_field=models.CharField(max_length=20))

    def __str__(self):
        return self.description

    def __repr__(self):
        return '<Data Function %s | %s | %s>' % (self.description, self.type, self.boundary)


class TransactionalFunction(Countable):
    TRANS_FUNCTION_TYPE = [
        ('EI', 'EI | External Input'),
        ('EO', 'EO | External Output'),
        ('EQ', 'EQ | External Query')
    ]
    type = models.CharField(max_length=3, choices=TRANS_FUNCTION_TYPE, null=False)
    file_type_referenced = models.ManyToManyField('DataFunction')

    def __str__(self):
        return self.description

    def __repr__(self):
        return '<Transactional Function %s | %s | %s>' % (self.description, self.type, self.boundary)


class Counting(models.Model):
    project = models.ForeignKey('Project', on_delete=models.DO_NOTHING, blank=False, null=False)
    date = models.DateField(auto_now_add=False)
    description = models.CharField(max_length=100, null=True, blank=True)

    class Meta:
        constraints = [
            models.UniqueConstraint(fields=['description', 'project'], name='unique counting in project'),
        ]

    def save(self, *args, **kwargs):
        if self.pk:
            current_project = Counting.objects.get(pk=self.pk)
            if current_project != self.project:
                raise DataError('Project can not be edited')
        super(Counting, self).save(*args, **kwargs)

    def __str__(self):
        return self.description

    def __repr__(self):
        return '<Counting: %s | %s>' % (self.description, self.project)


class OperationType(models.Model):
    PARAMS_TYPE = [('FL', 'files'),
                   ('RC', 'records'),
                   ('CN', 'countable')]
    description = models.CharField(max_length=50, null=False)
    short_description = models.CharField(max_length=3, null=False)
    params = models.CharField(max_length=2, choices=PARAMS_TYPE, null=False)

    class Meta:
        constraints = [
            models.UniqueConstraint(fields=['description'], name='unique description'),
            models.UniqueConstraint(fields=['short_description'], name='unique short_description'),
        ]

    def __str__(self):
        return self.description

    def __repr__(self):
        return '<Operation Type: %s>' % self.description


class Operation(models.Model):
    counting = models.ForeignKey('Counting', on_delete=models.PROTECT, blank=False, null=False)
    type = models.ForeignKey('OperationType', on_delete=models.DO_NOTHING, null=False)
    countable = models.ForeignKey('Countable', on_delete=models.PROTECT, null=True)
    qtd = models.IntegerField('Quantity', null=True)
    fp = models.FloatField('Function Points', null=False)

